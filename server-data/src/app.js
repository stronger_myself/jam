const createError = require('http-errors')
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const typeorm = require('typeorm')

const app = express()

app.disable('x-powered-by')
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.get('/', (req, res) => {
	res.send('hello')
})

app.use((req, res, next) => {
	next(createError(404))
})

app.use((err, req, res, next) => {
	res.locals.message = err.message
	res.locals.error = req.app.get('env') === 'development' ? err : {}
	res.status(err.status || 500)
	res.end()
})

const initDatabse = async () => {
	try {
		const connection = await typeorm.createConnection()
		if (connection.isConnected) {
			console.log('Datebase is connected')
		}
	} catch (error) {
		throw new Error(`ERROR: Server run: ${error}`)
	}
}

module.exports = { app, initDatabse }
