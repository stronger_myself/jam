# JavaScript Enterprise 
## JAN - JavaScript API Markup
---

#### Applications: 
1. `client-private` 
1. `client-public` 
1. `server-data` 
--- 
#### Worker modes: 
1. `development | dev` 
1. `production | prod` 
1. `build` 
1. `test` 
--- 
#### Containers: 
- `node npm yarn` 
- `mysql` 
- `redis` 
- `nginx` 
--- 
#### About: 
- Фронтовый сервер-файлик: 
    - отдает статику (основное) 
    - запрашивает даные (proxy)(ssr) 
    - рендеринг (ssr) 
    - Аутентификация | Авторизация (proxy)(isomorphic) 
- Сервер данных: 
    - Моделирование данных (orm) 
    - Обработка запросов (graphql) 
    - Сбор ответа (graphql) 
    - Запрос к базам данных (orm) 
    - Аутентификация | Авторизация (passport) 
    - Идентификация | Сессии (cookie, redis) 
- Прокси сервер (Nginx) 
    - общение с внешним миром
    - проксирование внутренних запросов
--- 
#### Technical tasks: 
1. Запуск всех приложений `make file` 
    - `docker-compose up` (`npm start` или `yarn start`) 
    - `docker-compose up --mode=dev` (`npm run dev` или `yarn dev`) 
    - `docker-compose up --mode=build` (`npm build` или `yarn build`) 
1. Браузерное обновление фронтовых приложений 
1. Работа с кодом по `VS Code Remote Container` 
1. Локальное проксирования запросов  
--- 
#### Issue 
1. Доменные зоны (`api.site.ru` `admin.site.ru` `site.ru` ...) 
1. Продакшн режим в контейнере 
1. Деплой в контейнерах 
